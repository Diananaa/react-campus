import React from 'react'
import { StyleSheet, Text, View, TouchableOpacity } from 'react-native'
import { colors, fonts } from '../../../utils'


const Link = ({ tittle, size, align, onPress }) => {
  if (tittle === 'ReadMore') {
    return (
      <TouchableOpacity  onPress={onPress}>
        <Text style={styles.read} >Read More</Text>
      </TouchableOpacity>
    )
  }
  return (
    <TouchableOpacity onPress={onPress}>
      <Text style={styles.text(size, align)}>{tittle}</Text>
    </TouchableOpacity>
  )
}
export default Link;

const styles = StyleSheet.create({
  read: {
    color: colors.text.menuActive,
    textDecorationLine: 'underline',

  },
  text: (size, align) => ({
    fontSize: size,
    textAlign: align,
    color: colors.text.secondary,
    fontFamily: fonts.primary[400],
    textDecorationLine: 'underline',
  })
})
