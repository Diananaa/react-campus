import React from 'react'
import { StyleSheet, Text, View, TouchableOpacity, Alert } from 'react-native'
import { IconEdit, IconTrash } from '../../../assets'
import { colors } from '../../../utils'
import { Button, Gap } from '../../atoms'

const Header = ({ onPress, tittle }) => {
  
    return (
      <View style={styles.container}>
        <Button type="icon-only" icon="back-dark" onPress={onPress} />
        <Text style={styles.text}>{tittle}</Text>
        <Gap width={24} />
      </View>
    )
  }
  export default Header;

  const styles = StyleSheet.create({
    container: {
      paddingHorizontal: 16,
      paddingVertical: 15,
      backgroundColor: colors.white,
      flexDirection: 'row',
      alignItems: 'center'
    },
    text: {
      textAlign: 'center',
      flex: 1,
      fontFamily: 'Nunito-SemiBold',
      color: colors.text.primary
    }
  })
