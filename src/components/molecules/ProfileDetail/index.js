import React from 'react'
import { StyleSheet, Text, View, Image } from 'react-native'
import { ILNullPhoto, me, IconRemovePhoto } from '../../../assets'
import { colors, fonts } from '../../../utils'
import { Input, Gap, } from '../../../components'

const ProfileDetail = ({ nama, email, isRemove }) => {
    return (
        <View style={styles.container}>
            <View style={styles.photo}>
                <View style={styles.avatarWrapper}>
                    <Image style={styles.avatar} source={me} />
                    {isRemove && <IconRemovePhoto style={styles.removePhoto} />}
                </View>
                {nama && (
                    <View>
                        <Gap height={16} />
                        <Text style={styles.title}>{title}</Text>
                        <Text style={styles.email}>{email}</Text>
                    </View>
                )}
            </View>
        </View>
    )
}
export default ProfileDetail;

const styles = StyleSheet.create({
    removePhoto: {
        position: 'absolute',
        bottom: 8,
        right: 6
    },
    title: {
        fontSize: 24,
        color: colors.text.primary,
        fontFamily: fonts.primary[600],
        textAlign: 'center'
    },
    email: {
        fontSize: 18,
        fontFamily: fonts.primary.normal,
        textAlign: 'center',
        color: colors.text.secondary,
        marginTop: 4,
    },
    container: {
        paddingHorizontal: 40,
        backgroundColor: colors.white,
    },
    photo: {
        marginVertical: 20,
        alignItems: 'center',
        justifyContent: 'center'
    },
    avatar: {
        width: 110,
        height: 110,
        borderRadius: 110 / 2
    },
    avatarWrapper: {
        width: 130,
        height: 130,
        borderWidth: 1,
        borderColor: colors.border,
        borderRadius: 130 / 2,
        alignItems: 'center',
        justifyContent: 'center'
    },
})
