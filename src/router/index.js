import React from 'react'
import { createStackNavigator } from '@react-navigation/stack'
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import {
    Home,
    Maps,
    Profile
} from '../pages/user'
import {
    Maps_,
    Profile_
} from '../pages/umum'

import { BottomNavigator } from '../components/molecules';
import {
    DetailMaps,
    DetailMapsUmum,
    DetailPhoto,
    DetailRoom,
    DetailRoomUmum,
    Developer,
    EditProfile,
    GetStarted,
    ListFasilitasUmum,
    ListLembaga,
    ListRuangan,
    ListUnit,
    Login,
    Register,
    SearchRoom,
    Splash,
    UploadPhoto
} from '../pages';
import Home_ from '../pages/umum/Home_';

const Stack = createStackNavigator();
const Tab = createBottomTabNavigator()

const MainAppUser = () => {
    return (
        <Tab.Navigator tabBar={props => <BottomNavigator {...props} />}>
            <Tab.Screen name="Home" component={Home} />
            <Tab.Screen name="Maps" component={Maps} />
            <Tab.Screen name="Profile" component={Profile} />
        </Tab.Navigator>
    )
}

const MainAppUmum = () => {
    return (
        <Tab.Navigator tabBar={props => <BottomNavigator {...props} />}>
            <Tab.Screen name="Home" component={Home_} />
            <Tab.Screen name="Maps" component={Maps_} />
            <Tab.Screen name="Profile" component={Profile_} />
        </Tab.Navigator>
    )
}

const Router = () => {
    return (
        <Stack.Navigator initialRouteName="MainAppUmum">
            <Stack.Screen name="MainAppUser" component={MainAppUser} options={{ headerShown: false }} />
            <Stack.Screen name="MainAppUmum" component={MainAppUmum} options={{ headerShown: false }} />
            <Stack.Screen name="Splash" component={Splash} options={{ headerShown: false }} />
            <Stack.Screen name="GetStarted" component={GetStarted} options={{ headerShown: false }} />
            <Stack.Screen name="Login" component={Login} options={{ headerShown: false }} />
            <Stack.Screen name="Register" component={Register} options={{ headerShown: false }} />
            <Stack.Screen name="UploadPhoto" component={UploadPhoto} options={{ headerShown: false }} />
            <Stack.Screen name="Developer" component={Developer} options={{ headerShown: false }} />
            <Stack.Screen name="EditProfile" component={EditProfile} options={{ headerShown: false }} />
            <Stack.Screen name="DetailRoom" component={DetailRoom} options={{ headerShown: false }} />
            <Stack.Screen name="DetailPhoto" component={DetailPhoto} options={{ headerShown: false }} />
            <Stack.Screen name="DetailMaps" component={DetailMaps} options={{ headerShown: false }} />
            <Stack.Screen name="DetailMapsUmum" component={DetailMapsUmum} options={{ headerShown: false }} />
            <Stack.Screen name="DetailRoomUmum" component={DetailRoomUmum} options={{ headerShown: false }} />
            <Stack.Screen name="SearchRoom" component={SearchRoom} options={{ headerShown: false }} />
            <Stack.Screen name="ListFasilitasUmum" component={ListFasilitasUmum} options={{ headerShown: false }} />
            <Stack.Screen name="ListLembaga" component={ListLembaga} options={{ headerShown: false }} />
            <Stack.Screen name="ListUnit" component={ListUnit} options={{ headerShown: false }} />
            <Stack.Screen name="ListRuangan" component={ListRuangan} options={{ headerShown: false }} />
        </Stack.Navigator>
    )
}
export default Router