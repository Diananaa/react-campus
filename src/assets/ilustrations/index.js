import ILLogo from './logo.svg';
import ILUpr from './upr.png'
import ILGetStarted from './get-started.png'
import ILBgUPR from './bg1.png'
import ILNullPhoto from './null-photo.png'
import ILHomePage from './homePage.png'
import ILRoom from './room.png'

export { 
    ILLogo, 
    ILGetStarted, 
    ILUpr, 
    ILBgUPR, 
    ILNullPhoto,
    ILHomePage,
    ILRoom 
};