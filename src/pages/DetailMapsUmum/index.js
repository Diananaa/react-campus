import React, { useEffect, useState } from 'react'
import { Alert, PermissionsAndroid, StyleSheet, View } from 'react-native'
import Geolocation from 'react-native-geolocation-service'
import MapView, { PROVIDER_GOOGLE } from 'react-native-maps'

const DetailMapsUmum = ({ navigation }) => {
  const [room, setRoom] = useState([])
  const [lang, setLang] = useState([])

  useEffect(() => {
    permission();

    Alert.alert(
      "You must login your account",
      `Are you haven't account?`,
      [
        {
          text: 'NO', style: 'cancel',
          onPress: () => {
            navigation.goBack()
          }
        },
        {
          text: 'YES', onPress: () => {

            navigation.replace('Login')
          }
        },
      ]
    );

  }, [])

  const permission = async () => {
    try {
      const granted = await PermissionsAndroid.request(
        PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION,
        {
          title: "Allow 'Location' to access your location",
          message:
            "This app need access to your location! ",
          buttonNeutral: "Ask Me Later",
          buttonNegative: "Don't Allow",
          buttonPositive: "Allow"
        }
      );
      if (granted === PermissionsAndroid.RESULTS.GRANTED) {
        console.log("Access your location");

        Geolocation.getCurrentPosition(
          (position) => {
            console.log(position);
          },
          (error) => {
            console.log(error.code, error.message);
          },
          {
            enableHighAccuracy: true,
            timeout: 3000,
            maximumAge: 10000,
            interval: 1000,
            accuracy: {
              android: 'high'
            },
            forceRequestLocation: true
          }
        );

      } else {
        console.log("Access location permission denied");
      }
    } catch (err) {
      console.warn(err);

    }

  }

  return (

    <View style={styles.content}>
      <View style={{ flex: 1 }}>
        <MapView
          provider={PROVIDER_GOOGLE}
          style={{ flex: 1 }}
          initialRegion={{
            latitude: -2.2158532,
            longitude: 113.8985776,
            latitudeDelta: 0.01,
            longitudeDelta: 0.01,
          }}
        >
          {/* {
            listRoom.map((item, index) =>
              < Marker
                coordinate={item.coordinate}
                title={item.title}
                description={item.desc}
              />
            )
          } */}
        </MapView>
      </View>
    </View>

  )
}
export default DetailMapsUmum;

const styles = StyleSheet.create({
  refresh: {
    width: 30,
    height: 25,
    justifyContent: 'center',
    alignContent: 'center',
    alignItems: 'center',
    zIndex: 10,
    position: 'absolute',
    top: 60,
    left: 4
  },
  content: {
    flex: 1,
  },
  searc: {
    zIndex: 3
  }
})
