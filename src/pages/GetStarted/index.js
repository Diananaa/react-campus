import React from 'react'
import { Image, ImageBackground, StyleSheet, Text, View } from 'react-native'
import { ILGetStarted, ILUpr } from '../../assets'
import { Button, Gap } from '../../components'
import { colors, fonts } from '../../utils'

const GetStarted = ({ navigation }) => {
  return (
    <ImageBackground source={ILGetStarted} style={styles.page}>
      <View>
        <Image style={{ height: 100, width: 100 }} source={ILUpr} />

        <Text style={styles.tittle}>introduction to the campus at University of Palangka Raya</Text>

      </View>

      <View>
        <Button tittle="Get Started" onPress={() => navigation.navigate('MainAppUmum')} />
        <Gap height={16} />
        <Button type="secondary" tittle="Sign In" onPress={() => navigation.replace('Login')} />
      </View>

    </ImageBackground>
  )
}
export default GetStarted;

const styles = StyleSheet.create({
  page: {
    flex: 1,
    padding: 40,
    backgroundColor: colors.white,
    justifyContent: 'space-between'
  },
  tittle: {
    fontSize: 28,
    marginTop: 91,
    color: colors.white,
    fontFamily: fonts.primary[600],
    width: 300,

  }
})
