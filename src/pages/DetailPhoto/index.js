import React from 'react'
import { StyleSheet, Text, View, Image } from 'react-native'


const DetailPhoto = ({ route }) => {
    const photo = route.params
    console.log(photo)
    return (
        <View style={styles.container}>
            <Image source={{ uri: photo}} style={{ flex: 1 }} />
        </View>
    )
}
export default DetailPhoto;

const styles = StyleSheet.create({
    container: {
        flex: 1
    }
})
