import React, { useEffect, useState } from 'react';
import { Image, ScrollView, StyleSheet, View } from 'react-native';
import MapView, { Marker, PROVIDER_GOOGLE } from 'react-native-maps';
import { useDispatch } from 'react-redux';
import { IconMapsUser } from '../../../assets';
import { Galeri, GaleriFasilitasUmum, ListDetailRoom, ListTile, Search } from '../../../components';
import { Fire } from '../../../config';
import { colors } from '../../../utils';

const Home = ({ navigation }) => {
  const dispatch = useDispatch()
  const [room, setRoom] = useState([])
  const [Unit, setUnit] = useState([])
  const [Lembaga, setLembaga] = useState([])
  const [ruangan, setRuangan] = useState([])

  useEffect(() => {
    fasilitas()
    unit()
    lembaga()
    Ruangan()
  }, [])

  const fasilitas = () => {
    dispatch({ type: 'SET_LOADING', value: true })
    Fire.database()
      .ref('rooms/')
      .orderByChild("Kategori")
      .equalTo('Fasilitas Umum')
      .limitToFirst(4)
      .once('value')
      .then(res => {
        console.log('data val listroom: ', res.val())
        if (res.val()) {
          const oldData = res.val()
          const data = []
          Object.keys(oldData).map((key) => {
            data.push({
              id: key,
              data: oldData[key]
            })
          })
          setRoom(data)
        }
        dispatch({ type: 'SET_LOADING', value: false })
      })
      .catch(err => {
        showError(err.message)
        dispatch({ type: 'SET_LOADING', value: false })
      })
    console.log('data rooms di dlm useEffect: ', room)
  }

  const unit = () => {
    dispatch({ type: 'SET_LOADING', value: true })
    Fire.database()
      .ref('rooms/')
      .orderByChild("Kategori")
      .equalTo('Unit')
      .limitToFirst(4)
      .once('value')
      .then(res => {
        console.log('data val listroom: ', res.val())
        if (res.val()) {
          const oldData = res.val()
          const data = []
          Object.keys(oldData).map((key) => {
            data.push({
              id: key,
              data: oldData[key]
            })
          })
          setUnit(data)
        }
        dispatch({ type: 'SET_LOADING', value: false })
      })
      .catch(err => {
        showError(err.message)
        dispatch({ type: 'SET_LOADING', value: false })
      })
    console.log('data unit di dlm useEffect: ', Unit)
  }

  const lembaga = () => {
    dispatch({ type: 'SET_LOADING', value: true })
    Fire.database()
      .ref('rooms/')
      .orderByChild("Kategori")
      .equalTo('Lembaga')
      .limitToFirst(4)
      .once('value')
      .then(res => {
        console.log('data val listroom: ', res.val())
        if (res.val()) {
          const oldData = res.val()
          const data = []
          Object.keys(oldData).map((key) => {
            data.push({
              id: key,
              data: oldData[key]
            })
          })
          setLembaga(data)
        }
        dispatch({ type: 'SET_LOADING', value: false })
      })
      .catch(err => {
        showError(err.message)
        dispatch({ type: 'SET_LOADING', value: false })
      })
    console.log('data Lembaga di dlm useEffect: ', Lembaga)
  }

  const Ruangan = () => {
    dispatch({ type: 'SET_LOADING', value: true })
    Fire.database()
      .ref('rooms/')
      .orderByChild("Kategori")
      .equalTo('Fakultas')
      .limitToFirst(4)
      .once('value')
      .then(res => {
        console.log('data val listroom: ', res.val())
        if (res.val()) {
          const oldData = res.val()
          const data = []
          Object.keys(oldData).map((key) => {
            data.push({
              id: key,
              data: oldData[key]
            })
          })
          setRuangan(data)
        }
        dispatch({ type: 'SET_LOADING', value: false })
      })
      .catch(err => {
        showError(err.message)
        dispatch({ type: 'SET_LOADING', value: false })
      })
    console.log('data Lembaga di dlm useEffect: ', ruangan)
  }
  return (
    <>
      <Search onPress={() => navigation.navigate('SearchRoom')} />
      <ScrollView showsVerticalScrollIndicator={false}>
        <View >

          {/* Maps */}
          <View style={{ height: 200, width: '100%', borderColor: colors.border1.sec, borderBottomWidth: 1 }}>
            <MapView
              provider={PROVIDER_GOOGLE}
              style={{ flex: 1 }}
              initialRegion={{
                latitude: -2.2158532,
                longitude: 113.8985776,
                latitudeDelta: 0.01,
                longitudeDelta: 0.01,
              }}
            >
              <Marker
                coordinate={{ latitude: latitude, longitude: longitude }}
                title="My Location"
              >
                <Image source={IconMapsUser} style={{ height: 30, width: 30 }} />
              </Marker>
            </MapView>
          </View>

          <View style={styles.page}>
            <View>
              <ListTile title='Fasilitas Umum' onPress={() => navigation.navigate('ListFasilitasUmum')} />
              <ScrollView horizontal showsHorizontalScrollIndicator={false}>
                {room.map((item, index) => {
                  return (
                    <GaleriFasilitasUmum
                      key={index}
                      gambar={item.data.Gambar}
                      ruangan={item.data.Ruangan}
                      onPress={() => navigation.navigate('DetailRoom', { item })}
                    />
                  )
                })}
              </ScrollView>
            </View>

            <View>
              <ListTile title='Unit' onPress={() => navigation.navigate('ListUnit')} />
              <ScrollView horizontal showsHorizontalScrollIndicator={false}>
                {
                  Unit.map((item, index) => {
                    return (
                      <Galeri
                        key={index}
                        gambar={item.data.Gambar}
                        ruangan={item.data.Ruangan}
                        onPress={() => navigation.navigate('DetailRoom', { item })}
                      />
                    )
                  })
                }

              </ScrollView>
            </View>

            <View>
              <ListTile title='Lembaga' onPress={() => navigation.navigate('ListLembaga')} />
              <ScrollView horizontal showsHorizontalScrollIndicator={false}>
                {
                  Lembaga.map((item, index) => {
                    return (
                      <Galeri
                        key={index}
                        gambar={item.data.Gambar}
                        ruangan={item.data.Ruangan}
                        onPress={() => navigation.navigate('DetailRoom', { item })}
                      />
                    )
                  })
                }
              </ScrollView>
            </View>

            <View style={styles.card}>
              <ListTile title="Ruangan" onPress={() => navigation.navigate('ListRuangan')} />
              {/* <ListDetailRoom /> */}
              {
                ruangan.map((item, index) => {
                  return (
                    <ListDetailRoom
                      key={index}
                      gambar={item.data.Gambar}
                      ruangan={item.data.Ruangan}
                      deskripsi={item.data.Deskripsi}
                      onPress={() => navigation.navigate('DetailRoom', { item })}
                    />
                  )
                })}
            </View>

          </View>



          <View style={styles.page}>
            {/* {room.map((item, index) => {
              return (
                <ListDetailRoom
                  key={index}
                  gambar={item.data.Gambar}
                  kategori={item.data.Kategori}
                  prodi={item.data.Prodi}
                  ruangan={item.data.Ruangan}
                  deskripsi={item.data.Deskripsi}
                  onPress={() => navigation.navigate('DetailRoom', { item })}
                />
              )
            })} */}
          </View>


        </View>
      </ScrollView>
    </>
  )
}
export default Home;

const styles = StyleSheet.create({
  img: {
    height: 24,
    width: 230,
  },
  textSearch: {
    color: "#9FA5AF",
    flex: 1,
  },
  search: {
    flexDirection: 'row',
    borderWidth: 1,
    borderColor: colors.border1.sec,
    borderRadius: 10,
    marginVertical: 8,
    marginHorizontal: 24,
    paddingHorizontal: 8,
    alignItems: 'center',
    height: 40,
    paddingHorizontal: 8,
  },
  page: {
    marginVertical: 8,
    marginHorizontal: 24,
  },
  container: {

    alignContent: 'center',
    alignItems: 'center',
  },
  menu: {
    flexDirection: 'row',
    flex: 1,

  }
})
