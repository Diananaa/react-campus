import React, { useState, useEffect } from 'react'
import {
  StyleSheet,
  Text,
  View,
  Image,
  ScrollView,
  Alert
} from 'react-native'
import {
  ProfileDetail,
  Search, Input, Gap, List,
} from '../../../components'
import { ILHomePage, ILNullPhoto } from '../../../assets';
import { colors, getData, showError } from '../../../utils';
import { Fire } from '../../../config'

const Profile = ({ navigation }) => {
  const [profile, setProfile] = useState({
    name: "",
    email: ''
  })

  const signOut = () => {
    Alert.alert(
      "Logging out",
      `Are you sure`,
      [
        { text: 'NO', style: 'cancel' },
        {
          text: 'YES', onPress: () => {
            Fire.auth()
              .signOut()
              .then(res => {
                console.log('success sign out')
                navigation.replace('GetStarted')
              }).catch(err => {
                showError(err.message)
              })
          }
        },
      ]
    );

  }

  return (
    <View style={styles.container}>
      {profile.name.length > 0 && (
        <ProfileDetail nama={profile.name} email={profile.email} />
      )}
      <Gap height={10} />
      <List
        name="Sign Out"
        desc="You out from this account"
        type="next"
        icon="logout"
        onPress={() => signOut()}
      />
      <List
        name="Developer"
        desc="Detail about developer"
        type="next"
        icon="dev"
        onPress={() => navigation.navigate('Developer')}
      />
    </View>
  )
}
export default Profile;

const styles = StyleSheet.create({
  container: {
    paddingHorizontal: 40,
    backgroundColor: colors.white,
    flex: 1
  }
})
