import GetStarted from './GetStarted'
import Login from './Login'
import Register from './Register'
import Splash from './Splash'
import UploadPhoto from './UploadPhoto'
import Developer from './Developer'
import EditProfile from './EditProfile'
import DetailRoom from './DetailRoom'
import DetailPhoto from './DetailPhoto'
import DetailMaps from './DetailMaps'
import DetailRoomUmum from './DetailRoomUmum' 
import DetailMapsUmum from './DetailMapsUmum'
import SearchRoom from './SearchRoom'
import ListFasilitasUmum from './ListFasilitasUmum'
import ListUnit from './ListUnit'
import ListLembaga from './ListLembaga'
import ListRuangan from './ListRuangan'

export{
    ListRuangan,
    ListFasilitasUmum,
    ListUnit,
    ListLembaga,
    SearchRoom,
    EditProfile,
    Developer,
    GetStarted,
    Login,
    Register,
    Splash,
    UploadPhoto,
    DetailRoom,
    DetailPhoto,
    DetailMaps,
    DetailRoomUmum,
    DetailMapsUmum
}