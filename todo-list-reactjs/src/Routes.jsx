import { BrowserRouter, Route, Routes } from "react-router-dom";
import { Add, Edit, Home } from "./pages";

export default function AppRoutes() {
  return (
    <BrowserRouter>
      <Routes>
        <Route path="/" element={<Home />} />
        <Route path="/add" element={<Add />} />
        <Route path="/edit:id" element={<Edit />} />
      </Routes>
    </BrowserRouter>
  );
}
