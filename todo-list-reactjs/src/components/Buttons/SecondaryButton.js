import React from "react";

export default function SecondaryButton({
  text,
  onClickFunction,
  additionalStyles = ``,
  ...rest
}) {
  return (
    <button
      className={`${additionalStyles} h-10 py-2 px-3 text-black outline-primary-05 outline rounded-xl`}
      onClick={onClickFunction}
      {...rest}
    >
      {text}
    </button>
  );
}
