import React from "react";
import PrimaryButton from "../Buttons/PrimaryButton";

export default function Navbar() {
  return (
    <div className="justify-items-center shadow-high">
      <div className="container mx-auto  sm:px-[30px] py-[10px] flex justify-between  w-full">
        <p className="text-lg font-medium">Todo List</p>
        <PrimaryButton text={"Tambah"} to={"/add"} />
      </div>
    </div>
  );
}
