import React from "react";

const LabelTextareainput = ({ label, ...rest }) => {
  const labelStyle = "text-sm sm:text-base lg:text-base";
  const inputStyle =
    "text-sm sm:text-base lg:text-base rounded-lg px-3 py-1 border-2 w-full mt-1";
  const containerStyle = "mb-2 lg:mb-5";

  return (
    <div className={containerStyle}>
      <label className={labelStyle}>{label}</label>
      <br />
      <textarea rows="3" className={inputStyle} {...rest} />
    </div>
  );
};

export default LabelTextareainput;
