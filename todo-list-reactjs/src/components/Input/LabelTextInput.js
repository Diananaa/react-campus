const LabelTextInput = ({ label, ...rest }) => {
  const labelStyle = "text-sm sm:text-base lg:text-base";
  const inputStyle =
    "text-sm sm:text-base lg:text-base rounded-lg px-3 py-1 border-2 w-full mt-1";
  const containerStyle = "mb-2 lg:mb-5 ";
  return (
    <div className={containerStyle}>
      <label className={labelStyle}>{label}</label>
      <br />
      <input className={inputStyle} type="text" {...rest} />
    </div>
  );
};

export default LabelTextInput;
