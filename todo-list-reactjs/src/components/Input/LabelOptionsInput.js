import React from "react";
import { stringToObject } from "../../constants/stringToObject";

export default function LabelOptionsInput({
  id,
  label,
  defaultValue,
  value,
  values = [],
  valueOption,
  valueName,
  ...rest
}) {
  const labelStyle = "text-sm sm:text-base lg:text-base";
  const containerStyle = "mb-2 lg:mb-5";
  return (
    <div className={containerStyle}>
      <label className={labelStyle}>{label}</label>
      <br />
      <select
        {...rest}
        className={`rounded-lg px-3 py-1 border-2 w-full mt-1 ${
          value === "default" && "text-neutral-03"
        } w-full focus:outline-none`}
        data-testid={id}
        id={id}
        value={value || "default"}
      >
        <option className="text-neutral-100" disabled value="default">
          {defaultValue}
        </option>
        {values?.map((value, i) => (
          <option key={i} value={value.stringToObject(valueOption)}>
            {value.stringToObject(valueName)}
          </option>
        ))}
      </select>
    </div>
  );
}
