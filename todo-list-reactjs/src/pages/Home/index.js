import React from "react";
import { useSelector } from "react-redux";
import { Card, Loading, Navbar } from "../../components";

export default function Home() {
  const task = useSelector((state) => state.taskReducer);
  const isLoading = useSelector((state) => state.globalReducer.isLoading);
  return (
    <>
      <Navbar />
      {isLoading === true ? (
        <Loading />
      ) : (
        <>
          {Object.entries(task.byIds).length < 1 ? (
            <p>Nothing Task</p>
          ) : (
            Object.entries(task.byIds).map(([id, data], i) => (
              <Card data={data} id={id} key={i} />
            ))
          )}
        </>
      )}
    </>
  );
}
