import React from "react";
import { useNavigate } from "react-router-dom";

export default function ProductLayout({ button, title, children }) {
  const navigate = useNavigate();
  return (
    <>
      <div className=" container mx-auto flex items-center sm:px-[30px] py-[10px] shadow-high">
        <div>
          <button onClick={() => navigate(-1)}>
            <img src="/icons/fi_arrow-left.svg" alt="back" />
          </button>
        </div>
        <div className="flex justify-center w-full">
          <p className="text-lg font-semibold">{title}</p>
        </div>
        <div>{button}</div>
      </div>
      <div>{children}</div>
    </>
  );
}
