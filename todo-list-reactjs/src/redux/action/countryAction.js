import * as countryAPI from "../../services/api/countryAPI";

export const countryAction = () => (dispatch) => {
  countryAPI
    .country()
    .then((res) => {
      dispatch({ type: "SET_COUNTRY", payload: res.data.data });
    })
    .catch((err) => console.log(err));
};
