const InitTaskState = {
  task: [],
  byIds: {},
};

const taskReducer = (state = InitTaskState, action) => {
  switch (action.type) {
    case "ADD_TASK":
      const content = action.content;
      const id = action.payload;
      return {
        ...state,
        task: [...state.task, id],
        byIds: {
          ...state.byIds,
          [id]: {
            content,
            completed: false,
          },
        },
      };
    case "UPDATE_TASK":
      const { no } = action.payload;
      return {
        ...state,
        byIds: {
          ...state.byIds,
          [no]: {
            ...state.byIds[no],
            content: action.content,
          },
        },
      };
    case "DELETE_TASK":
      console.log("isi reducer", action.payload);
      const { idDelete } = action.payload;
      const { [idDelete]: remove, ...newIds } = state.byIds;

      return {
        ...state,
        byIds: newIds,
      };

    case "TOGGLE_TODO": {
      const { id } = action.payload;
      return {
        ...state,
        byIds: {
          ...state.byIds,
          [id]: {
            ...state.byIds[id],
            completed: !state.byIds[id].completed,
          },
        },
      };
    }
    default:
      return state;
  }
};
export default taskReducer;
